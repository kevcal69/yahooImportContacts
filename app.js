let express = require('express')
let nunjucks = require('nunjucks')

let app = express()

app.set('view engine', nunjucks)

nunjucks.configure(__dirname + '/templates',
    {
        autoescape: true,
        express: app
    })

app.engine('html', nunjucks.render)
app.use('/static', express.static(__dirname + '/static'))

// Add headers
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', '"Origin, X-Requested-With, Content-Type, Accept"');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

app.get('/', (req, res) => {
    res.render('index.html')
})

app.listen(8080, _ => {
    console.log('listening to port 3k')
})
